/**
 * Copyright 2015-2016 Edu Garcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arcnor.sp.parser

import com.arcnor.sp.data.*
import org.antlr.v4.runtime.ANTLRInputStream
import org.antlr.v4.runtime.CommonTokenStream
import org.junit.Assert.assertEquals
import org.junit.Test

class ParserTests {
	@Test
	public fun testMult() {
		doTest("4*2", 4L * 2L)
	}

	@Test
	public fun testDiv() {
		doTest("4/2", 4L / 2L)
	}

	@Test
	public fun testMultDiv() {
		doTest("4*2/8", 4L * 2L / 8L)
	}

	@Test
	public fun testPrecedence() {
		doTest("2 + ~5 + 4 * 2", 2L + 5L.inv() + 4 * 2)
		doTest("2 + 3 + 4 * 5", 2L + 3 + 4 * 5)
	}

	@Test
	public fun testMod() {
		doTest("432%23", 432L % 23L)
	}

	@Test
	public fun testParen() {
		doTest("8*(1+2/4)+10", 8 * (1 + 2 / 4) + 10L)
	}

	@Test
	public fun testShift() {
		doTest("(8<<3)+(5>>1)", (8L shl 3) + (5L shr 1))
	}

	@Test
	public fun testBool() {
		doTest("4*2==2*4", 4 * 2 == 2 * 4)
	}

	@Test
	public fun testBool2() {
		doTest("4*2!=1+2", 4 * 2 != 1 + 2)
	}

	@Test
	public fun testCompGTE() {
		doTest("5>=5", 5 >= 5)
		doTest("5>=4", 5 >= 4)
	}

	@Test
	public fun testCompGT() {
		doTest("5>5", 5 > 5)
		doTest("5>4", 5 > 4)
	}

	@Test
	public fun testCompLTE() {
		doTest("5<=5", 5 <= 5)
		doTest("5<=4", 5 <= 4)
	}

	@Test
	public fun testCompLT() {
		doTest("5<5", 5 < 5)
		doTest("5<4", 5 < 4)
	}

	@Test
	fun testBit() {
		doTest("5 | 0x13 & 0x03", 5L or (0x13 and 0x03))    // In Kotlin, there is no operator precedence when using functions, so bitwise operations never have precedence as they are
	}

	@Test
	fun testBit2() {
		doTest("0x13 & 0x03 | 5", 0x13L and 0x03 or 5)
	}

	@Test
	fun testBit3() {
		doTest("0x13 ^ 0x03 | 5", 0x13L xor 0x03 or 5)
	}

	@Test
	fun testUnaryNot() {
		doTest("!(5 > 4)", !(5 > 4))
	}

	@Test
	fun testUnaryBitNot() {
		doTest("~42", 42L.inv())
	}

	@Test
	fun testUnaryMinus() {
		doTest("-42", -42L)
	}

	@Test
	fun testStringEq() {
		doTest("\"ab\" == \"ab\"", true)
		doTest("\"ab\" == 42", false)
	}

	@Test
	fun testStringNeq() {
		doTest("\"ab\" != \"ba\"", true)
		doTest("\"ab\" != 42", true)
	}

	@Test
	fun testPaths1() {
		val context = SPValueMap(null)
		context["first"] = SPValueLong(42L)

		doTest("42 * \$first", 42 * (context["first"] as SPValueLong).value, context)
	}

	@Test
	fun testPaths2() {
		val context = SPValueMap(null)
		val dataFirst = SPValueMap(context)
		context["first"] = dataFirst
		dataFirst["second"] = SPValueLong(42L)

		doTest("42 * \$first.second", 42 * ((context["first"] as SPValueMap)["second"] as SPValueLong).value, context)
	}

	@Test
	fun testPaths3() {
		val context = SPValueMap(null)
		val dataFirst = SPValueMap(context)
		context["first"] = dataFirst
		val dataSecond = SPValueMap(dataFirst)
		dataFirst["second"] = dataSecond
		dataSecond["third"] = SPValueLong(42L)

		doTest("42 * \$first.second.third", 42 * (((context["first"] as SPValueMap)["second"] as SPValueMap)["third"] as SPValueLong).value, context)
	}

	@Test
	fun testPathsRelative() {
		val context = SPValueMap(null)
		val dataFirst = SPValueMap(context)
		context["first"] = dataFirst
		val dataSecond = SPValueMap(dataFirst)
		dataFirst["second"] = dataSecond
		dataSecond["du_45"] = SPValueLong(42L)

		doTest("42 * \$second.du_45", 42 * ((dataFirst["second"] as SPValueMap)["du_45"] as SPValueLong).value, dataFirst)
	}

	@Test
	fun testPathsParent() {
		val context = SPValueMap(null)
		val dataFirst = SPValueMap(context)
		context["first"] = dataFirst
		val dataSecond = SPValueMap(dataFirst)
		dataFirst["second"] = dataSecond
		dataSecond["third"] = SPValueLong(42L)

		doTest("42 * \$first.second.third", 42 * (((context["first"] as SPValueMap)["second"] as SPValueMap)["third"] as SPValueLong).value, dataSecond)
	}

	@Test
	fun testPathsRoot() {
		val context = SPValueMap(null)
		val dataFirst = SPValueMap(context)
		context["first"] = dataFirst
		val dataSecond = SPValueMap(dataFirst)
		dataFirst["second"] = dataSecond
		dataSecond["third"] = SPValueLong(42L)

		doTest("42 * \$root.first.second.third", 42 * (((context["first"] as SPValueMap)["second"] as SPValueMap)["third"] as SPValueLong).value, dataSecond)
	}

	@Test
	fun testProps() {
		val context = SPValueMap(null)
		context.setProp("offset", SPValueLong(42L))
		val dataFirst = SPValueMap(context)
		context["first"] = dataFirst
		val dataSecond = SPValueMap(dataFirst)
		dataFirst["second"] = dataSecond
		dataSecond["third"] = SPValueLong(42L)
		dataSecond.setProp("fileLength", SPValueLong(128L))

		doTest("42 * \$this#fileLength", 42 * 128L, dataSecond)
	}

	@Test
	fun testPathsAndProps() {
		val context = SPValueMap(null)
		context.setProp("offset", SPValueLong(42L))
		context.setProp("fileLength", SPValueLong(128L))
		val dataFirst = SPValueMap(context)
		dataFirst.setProp("myProp", SPValueLong(128L))
		context["first"] = dataFirst
		val dataSecond = SPValueMap(dataFirst)
		dataFirst["second"] = dataSecond
		dataSecond["third"] = SPValueLong(42L)

		doTest("42 * \$first#myProp", 42 * ((context["first"] as SPValueMap).getProp("myProp") as SPValueLong).value, dataSecond)
	}

	@Test
	fun testPropsShadowing() {
		val context = SPValueMap(null)
		context.setProp("superProp", SPValueLong(1234L))
		val dataFirst = SPValueMap(context)
		dataFirst.setProp("superProp", SPValueLong(5678L))
		context["first"] = dataFirst

		doTest("42 * \$this#superProp", 42L * 5678L, dataFirst)
	}

	@Test
	fun testPropsShadowing2() {
		val context = SPValueMap(null)
		context.setProp("superProp", SPValueLong(1234L))
		val dataFirst = SPValueMap(context)
		dataFirst.setProp("superProp", SPValueLong(5678L))
		context["first"] = dataFirst

		doTest("42 * \$this#superProp", 42L * 1234L, context)
	}

	@Test
	fun testArray() {
		val context = SPValueMap(null)
		val dataFirst = SPValueMap(context)
		context["first"] = dataFirst
		val dataSecond = SPValueArray(arrayOf(SPValueLong(1), SPValueLong(42)))
		dataFirst["second"] = dataSecond

		doTest("42 * \$first.second[1]", 42 * (((context["first"] as SPValueMap).value["second"] as SPValueArray).value[1] as SPValueLong).value, context)
	}

	@Test
	fun testArray2() {
		val context = SPValueMap(null)
		val dataFirst = SPValueMap(context)
		context["first"] = dataFirst
		val dataSecond = SPValueArray(arrayOf(SPValueLong(1), SPValueLong(42)))
		dataFirst["second"] = dataSecond
		val dataSecondB = SPValueArray(arrayOf(SPValueLong(43), SPValueLong(1985)))
		dataFirst["another"] = dataSecondB

		doTest("\$first.second[1] * \$first.another[0]", 42L * 43L, context)
	}

	@Test
	fun testArrayString() {
		val context = SPValueMap(null)
		val dataFirst = SPValueMap(context)
		context["first"] = dataFirst
		val dataSecond = SPValueArray(arrayOf(SPValueString("Yeah"), SPValueString("Yeah")))
		dataFirst["second"] = dataSecond
		val dataSecondB = SPValueArray(arrayOf(SPValueString("Yeah"), SPValueString("Noeh")))
		dataFirst["another"] = dataSecondB

		doTest("\$first.second[1] == \$first.another[0]", true, context)
	}

	@Test
	fun testArrayComplexExpr() {
		val context = SPValueMap(null)
		val dataFirst = SPValueMap(context)
		context["first"] = dataFirst
		val dataSecond = SPValueArray(arrayOf(SPValueLong(1), SPValueLong(42)))
		dataFirst["second"] = dataSecond

		doTest("42 * \$first.second[\$first.second[0]]", 42 * 42L, context)
	}

	@Test
	fun testArrayMixHash() {
		val context = SPValueMap(null)
		val dataFirst = SPValueMap(context)
		context["first"] = dataFirst
		val zeroValue = SPValueLong(1)
		zeroValue.setProp("bits", SPValueArrayBoolean(booleanArrayOf(false, false, true)))
		val dataSecond = SPValueArray(arrayOf(zeroValue, SPValueLong(42)))
		dataFirst["second"] = dataSecond

		doTest("\$first.second[0]#bits[2]", true, context)
	}

	private fun doTest(expr: String, expected: Any, context: SPValueMap? = null) {
		val result = parse(expr, context)
		assertEquals(expected.javaClass, (result as SPPrimitiveValue<Any>).value.javaClass)
		assertEquals(expected, result.value)
	}

	private fun parse(expr: String, context: SPValueMap? = null): SPValue {
		val lexer = SPLexer(ANTLRInputStream(expr))
		val tokens = CommonTokenStream(lexer);
		val p = SPParser(tokens);
		p.buildParseTree = true;
		val listener = SPParserListener(context ?: SPValueMap(null))
		p.addParseListener(listener);

		p.expr();
		return listener.result()
	}
}
