/**
 * Copyright 2015-2016 Edu Garcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arcnor.sp.data

import java.util.LinkedHashMap

class SPValueMap(val parent: SPValueMap?, valid: Boolean = true) : SPValueContainer<SPValue, String>(valid) {
	override val value = LinkedHashMap<String, SPValue>()

	val keys: Set<String>
		get() = value.keys

	override operator fun set(id: String, value: SPValue) = this.value.put(id, value)

	override operator fun get(id: String) = value[id]
}
