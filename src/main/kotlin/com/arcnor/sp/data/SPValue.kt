/**
 * Copyright 2015-2016 Edu Garcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arcnor.sp.data

abstract class SPValue(
		/**
		 * Used to specify that this SPValue holds a valid... value :). In some cases (like when importing), SPValues
		 * are invalid while they get created and before they are read from the binary data.
		 */
		var valid: Boolean = true
) {
	private val props = hashMapOf<String, SPValue>()

	val keysProps: Set<String>
		get() = props.keys

	fun getProp(id: String) = props[id]

	fun setProp(id: String, value: SPValue) = props.put(id, value)
}

abstract class SPValueContainer<V, K>(valid: Boolean) : SPValue(valid) {
	abstract val value: Any

	abstract operator fun set(id: K, value: V): V?
	abstract operator fun get(id: K): V?

	override fun toString(): String {
		val result = value.toString()

		return if (valid) result else "$result [INVALID]"
	}
}

abstract class SPPrimitiveValue<T>(valid: Boolean) : SPValue(valid) {
	abstract var value: T

	override fun toString(): String {
		val result = value.toString()

		return if (valid) result else "$result [INVALID]"
	}
}

class SPValueLong(override var value: Long, valid: Boolean = true) : SPPrimitiveValue<Long>(valid)
class SPValueDouble(override var value: Double, valid: Boolean = true) : SPPrimitiveValue<Double>(valid)
class SPValueBoolean(override var value: Boolean, valid: Boolean = true) : SPPrimitiveValue<Boolean>(valid)
class SPValueString(override var value: String, valid: Boolean = true) : SPPrimitiveValue<String>(valid)

// Arrays (my kingdom for templates!!)
// TODO: Maybe we should only have one type of array (Long) to simplify things, even if it wastes memory
class SPValueArrayBoolean(override var value: BooleanArray, valid: Boolean = true) : SPValueContainer<Boolean, Int>(valid) {
	override operator fun set(id: Int, value: Boolean): Boolean {
		this.value[id] = value
		return value
	}

	override operator fun get(id: Int) = value[id]
}

class SPValueArrayByte(override var value: ByteArray, valid: Boolean = true) : SPValueContainer<Byte, Int>(valid) {
	override operator fun set(id: Int, value: Byte): Byte {
		this.value[id] = value
		return value
	}

	override operator fun get(id: Int) = value[id]
}

class SPValueArrayShort(override var value: ShortArray, valid: Boolean = true) : SPValueContainer<Short, Int>(valid) {
	override operator fun set(id: Int, value: Short): Short {
		this.value[id] = value
		return value
	}

	override operator fun get(id: Int) = value[id]
}

class SPValueArrayInt(override var value: IntArray, valid: Boolean = true) : SPValueContainer<Int, Int>(valid) {
	override operator fun set(id: Int, value: Int): Int {
		this.value[id] = value
		return value
	}

	override operator fun get(id: Int) = value[id]
}

class SPValueArrayLong(override var value: LongArray, valid: Boolean = true) : SPValueContainer<Long, Int>(valid) {
	override operator fun set(id: Int, value: Long): Long {
		this.value[id] = value
		return value
	}

	override operator fun get(id: Int) = value[id]
}

class SPValueArrayFloat(override var value: FloatArray, valid: Boolean = true) : SPValueContainer<Float, Int>(valid) {
	override operator fun set(id: Int, value: Float): Float {
		this.value[id] = value
		return value
	}

	override operator fun get(id: Int) = value[id]
}

class SPValueArrayDouble(override var value: DoubleArray, valid: Boolean = true) : SPValueContainer<Double, Int>(valid) {
	override operator fun set(id: Int, value: Double): Double {
		this.value[id] = value
		return value
	}

	override operator fun get(id: Int) = value[id]
}

class SPValueArray(override var value: Array<SPValue>, valid: Boolean = true) : SPValueContainer<SPValue, Int>(valid) {
	override operator fun set(id: Int, value: SPValue): SPValue {
		this.value[id] = value
		return value
	}

	override operator fun get(id: Int) = value[id]
}
