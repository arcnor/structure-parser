/**
 * Copyright 2015-2016 Edu Garcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arcnor.sp.parser

import com.arcnor.sp.data.SPPrimitiveValue
import com.arcnor.sp.data.SPValue
import com.arcnor.sp.data.SPValueBoolean
import com.arcnor.sp.data.SPValueContainer
import com.arcnor.sp.data.SPValueDouble
import com.arcnor.sp.data.SPValueLong
import com.arcnor.sp.data.SPValueMap
import com.arcnor.sp.data.SPValueString
import org.antlr.v4.runtime.ParserRuleContext
import java.util.Stack

class SemanticError(message: String) : RuntimeException(message) {

}

class SPParserListener(private val context: SPValueMap) : SPBaseListener() {
	private val stack = Stack<SPValue>()

	/**
	 * Returns a Long or a Boolean
	 */
	fun result(): SPValue {
		if (stack.size != 1) {
			throw RuntimeException("Stack contains less or more than 1 element! ($stack)")
		}
		// FIXME: When stack is empty, throw exception
		return stack.pop()
	}

	// Operations

	private fun doOp(op: (o1: Long, o2: Long) -> Long) {
		val o2 = (stack.pop() as SPValueLong).value;
		val o1 = (stack.pop() as SPValueLong).value;
		stack.add(SPValueLong(op(o1, o2)))
	}

	private fun doBitOp(op: (o1: Long, o2: Int) -> Long) {
		val o2 = (stack.pop() as SPValueLong).value.toInt();
		val o1 = (stack.pop() as SPValueLong).value;
		stack.add(SPValueLong(op(o1, o2)))
	}

	private fun doBoolOp(op: (Any, Any) -> Boolean, negate: Boolean) {
		val o2 = (stack.pop() as SPPrimitiveValue<*>).value as Any
		val o1 = (stack.pop() as SPPrimitiveValue<*>).value as Any
		val result = op(o1, o2)

		stack.push(SPValueBoolean(if (negate) result.not() else result))
	}

	private fun doCompOp(greater: Boolean, equals: Boolean) {
		val o2 = (stack.pop() as SPValueLong).value;
		val o1 = (stack.pop() as SPValueLong).value;

		val result = if (greater) {
			if (equals) o1 >= o2 else o1 > o2
		} else {
			if (equals) o1 <= o2 else o1 < o2
		}

		stack.push(SPValueBoolean(result))
	}

	private fun doCondOp(condAnd: Boolean) {
		val o2 = (stack.pop() as SPValueBoolean).value;
		val o1 = (stack.pop() as SPValueBoolean).value;
		val result = if (condAnd) o1 && o2 else o1 || o2

		stack.push(SPValueBoolean(result))
	}

	// Parsing of expressions

	override fun exitUnaryExpr(ctx: SPParser.UnaryExprContext) {
		val o1 = (stack.pop() as SPPrimitiveValue<*>).value
		when (ctx.unary().op.type) {
			SPLexer.MINUS -> stack.push(SPValueLong((o1 as Long).unaryMinus()))
			SPLexer.NOT -> stack.push(SPValueBoolean((o1 as Boolean).not()))
			SPLexer.BIT_NOT -> stack.push(SPValueLong((o1 as Long).inv()))
		}
	}

	override fun exitMultExpr(ctx: SPParser.MultExprContext) {
		when (ctx.mult().op.type) {
			SPLexer.MUL -> doOp(Long::times)
			SPLexer.DIV -> doOp(Long::div)
			SPLexer.MOD -> doOp(Long::mod)
		}
	}

	override fun exitAddExpr(ctx: SPParser.AddExprContext) {
		when (ctx.add().op.type) {
			SPLexer.PLUS -> doOp(Long::plus)
			SPLexer.MINUS -> doOp(Long::minus)
		}
	}

	override fun exitShiftExpr(ctx: SPParser.ShiftExprContext) {
		when (ctx.shift().op.type) {
			SPLexer.SHL -> doBitOp(Long::shl)
			SPLexer.SHR -> doBitOp(Long::shr)
			SPLexer.USHR -> doBitOp(Long::ushr)
		}
	}

	override fun exitEqExpr(ctx: SPParser.EqExprContext) {
		when (ctx.eq().op.type) {
			SPLexer.EQ -> doBoolOp(Any::equals, false)
			SPLexer.NEQ -> doBoolOp(Any::equals, true)
		}
	}

	override fun exitCompExpr(ctx: SPParser.CompExprContext) {
		when (ctx.comp().op.type) {
			SPLexer.GT -> doCompOp(greater = true, equals = false)
			SPLexer.LT -> doCompOp(greater = false, equals = false)
			SPLexer.GTE -> doCompOp(greater = true, equals = true)
			SPLexer.LTE -> doCompOp(greater = false, equals = true)
		}
	}

	override fun exitBitExpr(ctx: SPParser.BitExprContext) {
		when (ctx.bitOp.type) {
			SPLexer.BIT_AND -> doOp(Long::and)
			SPLexer.BIT_XOR -> doOp(Long::xor)
			SPLexer.BIT_OR -> doOp(Long::or)
		}
	}

	override fun exitCondExpr(ctx: SPParser.CondExprContext) {
		when (ctx.condOp.type) {
			SPLexer.COND_AND -> doCondOp(true)
			SPLexer.COND_OR -> doCondOp(false)
		}
	}

	//	override fun exitFieldExpr(ctx: SPParser.FieldExprContext) {
	//		val obj = stack.pop()
	//
	//		stack.push(null)
	//	}
	override fun exitFieldExpr(ctx: SPParser.FieldExprContext) {
		val obj = popOrError(stack, ctx)
		stack.push(obj)
	}

	override fun exitFExprField(ctx: SPParser.FExprFieldContext) {
		if (ctx.parent is SPParser.FieldExprContext) {
			parseInitialSelector(ctx)
		} else {
			parseSelector(ctx)
		}
	}

	override fun exitFExprArray(ctx: SPParser.FExprArrayContext?) {
		val idx = stack.pop() as SPValueLong
		val obj = stack.pop() as SPValueContainer<*, Int>

		val value = obj[idx.value.toInt()]
		// FIXME: This is not enough, there might be arrays inside arrays, etc
		when (value) {
			is SPValue -> stack.push(value)
			is Boolean -> stack.push(SPValueBoolean(value))
			is Float, is Double -> stack.push(SPValueDouble((value as Number).toDouble()))
			is Number -> stack.push(SPValueLong(value.toLong()))
			else -> throw RuntimeException("Unknown value type: ${value?.javaClass}")
		}
	}

	override fun exitFExprHash(ctx: SPParser.FExprHashContext) {
		val id = ctx.hashSelector().ID().text.trim()
		val obj = popOrError(stack, ctx)
		stack.push(obj.getProp(id))
	}

	private fun parseInitialSelector(ctx: SPParser.FExprFieldContext) {
		var startObj = context
		val startingPath = ctx.field().ID().text.trim()
		when (startingPath) {
			"this" -> stack.push(startObj)
			"root" -> {
				while (startObj.parent!= null) {
					startObj = startObj.parent!!
				}
				stack.push(startObj)
			}
			else -> {
				while (startObj[startingPath] == null && startObj.parent != null) {
					startObj = startObj.parent!!
				}
				stack.push(startObj[startingPath])
			}
		}
	}

	private fun parseSelector(ctx: SPParser.FExprFieldContext) {
		val obj = stack.pop() as SPValueMap
		val id = ctx.field().ID().text.trim()
		stack.push(obj[id])
	}

	override fun exitValue(ctx: SPParser.ValueContext) {
		val num = ctx.NUMBER()
		val hex = ctx.HEX_NUMBER()
		val str = ctx.STRING()

		when {
			num != null -> stack.push(SPValueLong(num.text.toLong()))
			hex != null -> stack.push(SPValueLong(java.lang.Long.valueOf(hex.text.substring(2), 16)))
			str != null -> stack.push(SPValueString(str.text.substring(1, str.text.length - 1)))
		}
	}

	private fun popOrError(stack: Stack<SPValue>, ctx: ParserRuleContext): SPValue {
		if (stack.isEmpty()) {
			throw RuntimeException("\"${ctx.text}\" not found!")
		}
		return stack.pop()
	}
}
