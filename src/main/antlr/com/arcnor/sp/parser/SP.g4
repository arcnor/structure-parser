/**
 * Copyright 2015-2016 Edu Garcia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
grammar SP;

@header
{
package com.arcnor.sp.parser;
}

expr:
	unary expr                             # unaryExpr
	| expr mult expr                        # multExpr
	| expr add expr                         # addExpr
	| expr shift expr                       # shiftExpr
	| expr comp expr                        # compExpr
	| expr eq expr                          # eqExpr
	| expr bitOp='&' expr                   # bitExpr
	| expr bitOp='^' expr                   # bitExpr
	| expr bitOp='|' expr                   # bitExpr
	| expr condOp='&&' expr                 # condExpr
	| expr condOp='||' expr                 # condExpr
	| '$' fExpr                             # fieldExpr
	| value                                 # valueExpr
	| '(' expr ')'                          # parenExpr
	;

value: HEX_NUMBER | NUMBER | STRING;

fExpr:
	fExpr '.' fExpr         # fExprSelector
	| fExpr arraySelector   # fExprArray
	| fExpr hashSelector    # fExprHash
	| field                 # fExprField
	;

field: ID;
hashSelector: '#' ID;
arraySelector: '[' expr ']';

unary: op='-' | op='!' | op='~';
add: op='+' | op='-';
mult: op='*' | op='/' | op='%';
eq: op='==' | op='!=';
comp: op='<=' | op='<' | op='>=' | op='>' ;
shift: op='<<' | op='>>' | op='>>>';

NOT : '!' ;
BIT_NOT : '~' ;
SHL : '<<' ;
SHR : '>>' ;
USHR : '>>>' ;
GTE : '>=' ;
LTE : '<=' ;
GT : '>' ;
LT : '<' ;
COND_AND : '&&' ;
COND_OR : '||' ;
BIT_AND : '&' ;
BIT_XOR : '^' ;
BIT_OR : '|' ;
EQ : '==' ;
NEQ : '!=' ;
MUL : '*' ;
DIV : '/' ;
MOD : '%' ;
PLUS : '+' ;
MINUS : '-' ;
ID  : [a-zA-Z_] [a-zA-Z0-9_]* ;
HEX_NUMBER : '0x' [0-9a-fA-F]+ ;
NUMBER : [0-9]+ ('.' [0-9]+)? ;
STRING : '"' ('\\"' | ~["\r\n])* '"';
WS : [ \t\r\n]+ -> skip ;
